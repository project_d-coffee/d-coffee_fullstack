import type Store from "@/types/Store";
import http from "./axios";

function getStore() {
  return http.get("/stores");
}
function getStoreById(id: number) {
  return http.get(`/stores/${id}`);
}
function saveStore(Store: Store) {
  return http.post("/stores", Store);
}
function updateStore(id: number, Store: Store) {
  return http.patch(`/stores/${id}`, Store);
}
function deleteStore(id: number) {
  return http.delete(`/stores/${id}`);
}
export default { getStore, getStoreById, saveStore, updateStore, deleteStore };