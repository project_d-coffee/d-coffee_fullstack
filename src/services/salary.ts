import type Salary from "@/types/Salary";
import http from "./axios";

function getSalary() {
  return http.get("/summarysalary");
}
function saveSalary(SummarySalary: Salary) {
  return http.post("/summarysalary", SummarySalary);
}
function updateSalary(id: number, SummarySalary: Salary) {
  return http.patch(`/summarysalary/${id}`, SummarySalary);
}
function deleteSalary(id: number) {
  return http.delete(`/summarysalary/${id}`);
}
export default { getSalary, saveSalary, updateSalary, deleteSalary };