import type Material from "@/types/material";
import http from "./axios";

function getMaterial() {
  return http.get("/materials");
}
function saveMaterial(Material: Material) {
  return http.post("/materials", Material);
}
function updateMaterial(id: number, Employee: Material) {
  return http.patch(`/materials/${id}`, Employee);
}
function deleteMaterial(id: number) {
  return http.delete(`/materials/${id}`);
}

export default { getMaterial, saveMaterial, updateMaterial, deleteMaterial};