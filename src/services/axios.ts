import axios from "axios";
const httpImg = (import.meta.env.VITE_BACKEND_URL)
const instance = axios.create({
  baseURL: `${httpImg}`,
});
function delay(time: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
}

instance.interceptors.request.use(async function (config) {
  const token = localStorage.getItem("token");
  if(token){
    config.headers.Authorization = `Bearer ${token}`;
  }
  await delay(1000);
  return config;
  
});



export default instance;