import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import CheckinoutService from '@/services/checkinout'
import type Checkinout from '@/types/Checkinout'
import { useUserStore } from './User'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import { useAuthStore } from './auth'

export const useCheckinoutStore = defineStore('Checkinout', () => {
  const auth = useAuthStore();
  const UserStore = useUserStore();
  const Checkinout = ref<Checkinout[]>([]);
  const currentUser = ref('')
  const dialog = ref(false);
  const editedCheckinout = ref<Checkinout>({ date: "", time_in: 0, time_out: 0, total_hour: 0 });
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const stage = ref();

  async function getCheckinoutStage(){
    try{
 
      loadingStore.isLoading = true
      const res = await CheckinoutService.getCheckinoutStage(auth.account?.employee?.email!)
      stage.value = res.data
    }catch(e){
      console.log(e)
    }
    
    loadingStore.isLoading = false
  }


  async function GetCheckinout() {
    loadingStore.isLoading = true;
    try {
      const res = await CheckinoutService.getCheckinoutByEmp(auth.account?.employee?.email!);
      Checkinout.value = res.data
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล Schedule ได้")
    }
    loadingStore.isLoading = false;
  }
  async function saveCheckinout() {
    loadingStore.isLoading = true;
    try {
        const res = await CheckinoutService.saveCheckinout({empId:auth.account?.employee?.id!});
        console.log(res)

      dialog.value = false;
      await GetCheckinout();
    } catch (e) {

      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล Schedule ได้")
    }
    await getCheckinoutStage()
    loadingStore.isLoading = false;
  }

  async function Checkout(){
    try{
      loadingStore.isLoading = true
      const res = await CheckinoutService.updateCheckinout({empId:auth.account?.employee?.id!})
      await GetCheckinout();
    }catch(e){
      console.log(e)
    }
  
    await getCheckinoutStage()
    loadingStore.isLoading = false
  }

  async function deleteCheckinout(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await CheckinoutService.deleteCheckinout(id);
      await GetCheckinout();
      await UserStore.GetUserNotEmp();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล Schedule ได้")
    }
    loadingStore.isLoading = false;
  }
  function editCheckinout(Checkinout: Checkinout) {
    editedCheckinout.value = JSON.parse(JSON.stringify(Checkinout));
    dialog.value = true;
  }
  return { GetCheckinout, Checkinout, editCheckinout, deleteCheckinout, saveCheckinout,Checkout,stage, dialog, editedCheckinout,currentUser,getCheckinoutStage}
})