import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type OrderItem from '@/types/OrderItem'
import type Product from '@/types/Product'
import type Order from '@/types/Order'
import OrderService from '@/services/Order'
import CustomerService from "@/services/customer";
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
import { useProductStore } from './Product'
import type Customer from '@/types/Customer'
import { useCustomerStore } from './Customer'
import type Employee from '@/types/Employee'

const StrUser = localStorage.getItem("user");
const user = JSON.parse(StrUser!)
const store = localStorage.getItem("store")
const currentStore = JSON.parse(store!)
const loadingStore = useLoadingStore();
const messageStore = useMessageStore();
const productStore = useProductStore();
const CustomerStore = useCustomerStore();

export const useOrderStore = defineStore('Order', () => {
    const OrderItem = ref<OrderItem[]>([]);
    const Bill = ref<OrderItem[]>([]);
    const order = ref<Order[]>([]);
    const orderBill = ref<{ id: number, createdDate: string, time: string, total: number, recevied: number, changed: number, customer: Customer, employee: Employee, discount: number }>();
    const orderDetail = ref<{ amount: number, name: string, price: number }[]>([])
    const dialog = ref(false);
    const dialogBill = ref(false);
    const dialogBillShow = ref(false);
    const tmp = ref()
    const model = ref(false);
    const discount = ref<{ point: number, discount: number }>({ point: 0, discount: 0 })
    const total = ref(0)
    const netTotal = total.value - discount.value.discount
    const recevied = ref(0)
    const payment = ref("Cash");
    const member = ref<Customer>()
    const noMember = CustomerStore.noMember


    watch((dialog), (newDialog) => {
        if (!newDialog) {
            model.value = false
            tmp.value = null
            member.value = { tel: "" }
            discount.value = { point: 0, discount: 0 }

        }
    })

    watch(discount, (newDiscount) => {
        if (newDiscount) {
            discount.value = { point: 0, discount: 0 }
        }
    })

    watch(tmp, (newTmp) => {
        if (newTmp) {
            member.value = tmp.value
            discount.value = { point: 0, discount: 0 }
        }
    })

    function addOrderItem(item: Product) {
        const index = OrderItem.value.findIndex((orderItems) => orderItems.productId === item.id);
        if (index === -1) {
            Bill.value.push({ productId: item.id!, product: item, price: item.price, amount: 1, total: item.price, })
            //
            OrderItem.value.push({ productId: item.id!, amount: 1 })

            //
            total.value = total.value + item.price


        } else {
            OrderItem.value[index].amount++
            // 
            Bill.value[index].amount++
            Bill.value[index].total = Bill.value[index].amount * Bill.value[index].price!;

            //
            total.value = total.value + item.price;
        }
        productStore.dialogAdd = false;
    }
    function RemoveOrderItem(item: OrderItem) {
        const index = OrderItem.value.findIndex((orderItems) => orderItems.productId === item.productId);
        if (OrderItem.value[index].amount > 1) {
            OrderItem.value[index].amount--

            // 
            Bill.value[index].amount--
            Bill.value[index].total = Bill.value[index].amount * Bill.value[index].price!;

            total.value = total.value - item.price!;
        } else {
            btnRemoveOrderItem(item)
        }

    }

    function btnRemoveOrderItem(item: OrderItem) {
        const index = OrderItem.value.findIndex((orderItems) => orderItems.productId === item.productId);
        total.value = total.value - Bill.value[index].total!;
        OrderItem.value.splice(index, 1)
        Bill.value.splice(index, 1)

    }
    async function submitOrder() {
        loadingStore.isLoading = true;
        try {
            const Order = ref<Order>({ empId: user.employee.id, tel: member.value?.tel, orderItems: OrderItem.value, recevied: recevied.value, storeId: currentStore.id, payment: payment.value, discount: discount.value.discount })
            const resCus = await CustomerService.updateCustomer(member.value!.id!, { point: member.value?.point! - discount.value.point });
            const resOrder = await OrderService.saveOrder(Order.value);
            orderBill.value = resOrder.data
            orderDetail.value = resOrder.data.orderItems
            total.value = 0;
            Bill.value = [];
            OrderItem.value = [];
            recevied.value = 0
            payment.value = "Cash"
            dialog.value = false
            dialogBill.value = true
            discount.value = { point: 0, discount: 0 }
            CustomerStore.GetCustomers();

        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถชำระเงินได้")
        }
        loadingStore.isLoading = false;

    }

    async function getOrders() {
        loadingStore.isLoading = true;
        try {
            const res = await OrderService.getOrder();
            order.value = res.data;
        } catch (e) {
            console.log(e);
            messageStore.showError("ไม่สามารถดึงข้อมูล Order ได้")
        }
        loadingStore.isLoading = false;
    }

    async function deleteOrder(id: number) {
        try {
            const res = await OrderService.deleteOrder(id)
            await getOrders();
        } catch (error) {
            console.log(error)
            messageStore.showError("ไม่สามารถลบข้อมูล Order ได้")
        }
    }

    async function getOrderById(id: number) {
        loadingStore.isLoading = true;
        try {
            const res = await OrderService.getOrderById(id);
            orderBill.value = res.data;
            orderDetail.value = res.data.orderItems;
            dialogBill.value = true;
        } catch (e) {
            console.log(e)
        }
        loadingStore.isLoading = false;
    }
    const search = ref("");

    const showOrder = computed(() => {
        return order.value.sort().reverse().filter((item) => {
            return item.id?.toString().match(search.value);
        })
    })
    return { OrderItem, discount, addOrderItem, member, btnRemoveOrderItem, RemoveOrderItem, Bill, submitOrder, dialog, tmp, model, total, recevied, payment, getOrders, order, deleteOrder, dialogBill, getOrderById, search, showOrder, orderBill, orderDetail, dialogBillShow, netTotal, noMember }
})