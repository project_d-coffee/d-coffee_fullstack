import type Employee from "./Employee";

export default interface User {
    id?: number;
    name?: string;
    tel?: string;
    email?: string;
    role?: string;
    password?: string;
    employee?: Employee;
    image?:string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
  }