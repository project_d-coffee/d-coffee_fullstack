import type Bill from "./Bill";
import type Material from "./material";

export default interface BillDetail {
    id?:number;
    name?:string;
    amount?:number;
    price?:number;
    total?:number;
    bill?:Bill[]
    materail?:Material[];
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
    

}