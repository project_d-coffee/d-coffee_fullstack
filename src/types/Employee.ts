import type User from "./User";

export default interface Employee {
  id?: number;
  name: string;
  tel: string;
  email: string;
  position: string;
  address: string;
  user?: User;
  hourly_wage?:number
  createdDate?: Date;
  updatedDate?: Date;
  deletedDate?: Date;
  }