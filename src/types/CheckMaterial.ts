import type CheckMaterialDetail from "./CheckMaterialDetail";
import type Employee from "./Employee";

export default interface CheckMaterial {
  id?: number;
  date?: string;
  time?: string;
  employee?: Employee;
  employeeId?:number;
  check_material_detail?: CheckMaterialDetail[];
}
