import type Employee from "./Employee";
import type Salary from "./Salary";

export default interface Checkinout {
  id?: number;
  empId? :number
  date? : String;
  time_in?: number;
  time_out?: number;
  total_hour?: number;
  employee?: Employee;
  salary?: Salary;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}