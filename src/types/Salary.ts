export default interface Salary {
  id?: number;
  Date: string;
  workhour: Number;
  salary: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}