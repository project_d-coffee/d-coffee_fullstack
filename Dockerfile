# Use a Node.js image for building the backend app
FROM 18-alpine

# Set working directory
WORKDIR /app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
# RUN npm install

# Copy the rest of the backend source code
COPY . .

RUN npm install


# Rebuild bcrypt to match the container's architecture
# RUN npm rebuild bcrypt --build-from-source

# Expose backend port (e.g., 3000 for NestJS)
EXPOSE 80

# Start the backend server
CMD ["npm", "run", "dev","--host"]